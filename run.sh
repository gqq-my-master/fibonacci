#!/bin/bash

args=(
# Image Settings affect subsequent processing such as reading an image
-density 100 
-background white 
-delay 100  
# Define the GIF disposal image setting for images that are being created
# Somehow this option must appear before input images.
-dispose Background 
-loop 0 
spiral.pdf  
# image operators affect the image immediately.
# An operator is applied to the current image set and forgotten. 

# Composite the image over the background color.
-alpha remove
# Do not optimize transparency because it will make the background gray.
-layers optimize-frame
spiral.gif
)

magick "${args[@]}"
